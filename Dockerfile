# nginx Build inspired by lando

FROM nginx:alpine
LABEL Maintainer="Lee Pang <lee@2ton.com>" \
      Description="Lightweight nginx container based on Alpine Linux."

RUN apk --no-cache add shadow; \
    rm -rf /var/cache/apk/* && rm -rf /tmp/*

RUN set -x \
    && addgroup -g 82 -S www-data \
    && adduser -u 82 -D -S -G www-data www-data

WORKDIR /var/www